{{~>subtypes name=name}}
{{~#if [$ref]~}}
{{~#if description~}}
/// {{description}}
{{/if}}
pub type {{camelcase name suffix}} = {{>data_type required="true"}};
{{~else}}
    {{~#if (eq type "object")~}}
        {{~#if properties~}}

            {{~#if (not_empty (fetch_patterns properties))}}
                lazy_static::lazy_static! {
                {{~#each (fetch_patterns properties)}}
                    static ref {{shoutysnakecase ../name @key ../suffix}}_PATTERN: regex::Regex
                            = regex::Regex::new("{{this}}").expect("Regex for `{{../name}}{{../suffix}}`'s parameter `{{@key}}`");
                {{~/each}}
                }
            {{~/if}}

            {{~#each properties}}

            {{~#if (or
                (or
                    (and (pattern) (patterns ""))
                    (and
                         (or
                            (eq type "string")
                            (or (eq type "integer") (eq type "number"))
                         )
                         (or
                            (not_empty minLength)
                            (not_empty maxLength)
                         )
                    )
                )
                (or
                    (and
                         (or (eq type "integer") (eq type "number"))
                         (or (not_empty minimum) (not_empty maximum))
                    )
                    (and
                         (eq type "array")
                         (or
                            (not_empty (fetch_patterns items))
                            (or (not_empty minItems) (not_empty maxItems))
                         )
                    )
                )
            )}}

            fn deserialize_{{snakecase ../name @key ../suffix}}<'de, D>(d: D)
            -> Result<
                {{~#if (not (has ../required @key))}}
                Option<
                {{~/if}}
                    {{~#if (eq type "string")}}String{{~/if}}
                    {{~#if (eq type "integer")}}i64{{~/if}}
                    {{~#if (eq type "number")}}i64{{~/if}}
                    {{~#if (eq type "array")}}{{>data_type name=(camelcase ../name @key) required="true"}}{{~/if}}
                {{~#if (not (has ../required @key))}}
                >
                {{~/if}}
                , D::Error>
            where
                D: serde::de::Deserializer<'de>,
            {
                {{~#if (has ../required @key)}}
                let res: {{>data_type name=(camelcase ../name @key) required="true"}} =
                            {{~#if (eq type "string")}}String{{~/if}}
                            {{~#if (eq type "integer")}}i64{{~/if}}
                            {{~#if (eq type "number")}}i64{{~/if}}
                            {{~#if (eq type "array")}}Vec::<_>{{~/if}}::deserialize(d)?;
                {{>validation object=this name=../name required="true" key=@key suffix=../suffix var_name="res"}}
                {{~else}}
                let res: {{>data_type name=(camelcase ../name @key)}} = Option::<
                {{~#if (eq type "string")}}String{{~/if}}
                {{~#if (eq type "integer")}}i64{{~/if}}
                {{~#if (eq type "number")}}i64{{~/if}}
                {{~#if (eq type "array")}}Vec<_>{{~/if}}
                >::deserialize(d)?;
                if let Some(res) = res.as_ref() {
                {{>validation object=this name=../name required="false" key=@key suffix=../suffix var_name="res"}}
                {{~/if}}
                {{~#if (not (has ../required @key))}}
                }
                {{~/if}}

                Ok(res)
            }
            {{~/if}}
            {{~/each}}

            #[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
            #[serde(deny_unknown_fields)]
            pub struct {{camelcase name suffix}} {
            {{~#each properties}}
                #[serde(rename = "{{@key}}")]
                {{~#if (or
                    (or
                        (and (pattern) (patterns ""))
                        (and
                            (or
                                (eq type "string")
                                (or (eq type "integer") (eq type "number"))
                            )
                            (or
                                (not_empty minLength)
                                (not_empty maxLength)
                            )
                        )
                    )
                    (or
                        (and
                            (or (eq type "integer") (eq type "number"))
                            (or (not_empty minimum) (not_empty maximum))
                        )
                        (and
                            (eq type "array")
                            (or
                                (not_empty (fetch_patterns items))
                                (or (not_empty minItems) (not_empty maxItems))
                            )
                        )
                    )
                )}}
                {{~#if (not (has ../required @key))}}
                #[serde(default)]
                {{~/if}}
                #[serde(deserialize_with = "deserialize_{{snakecase ../name @key ../suffix}}")]
                {{/if}}
                {{~#if (has ../required @key)}}
                pub {{sanitize (snakecase @key)}}: {{>data_type name=(camelcase ../name @key) required="true"}},
                {{~else}}
                #[serde(skip_serializing_if = "Option::is_none")]
                pub {{sanitize (snakecase @key)}}: {{>data_type name=(camelcase ../name @key)}},
                {{~/if}}
            {{~/each}}
            {{~#if additionalProperties}}
                pub properties: serde_json::Map<String, serde_json::Value>,
            {{~/if}}
            }

            {{~#if (has this.[x-tags] "multipart")}}
            impl TryFrom<HashMap<String, Vec<u8>>> for {{camelcase name suffix}} {
                type Error = &'static str;

                fn try_from(mut data: HashMap<String, Vec<u8>>) -> Result<Self, Self::Error> {
                    Ok({{camelcase name suffix}} {
                        {{~#each properties}}
                            {{sanitize (snakecase @key)}}: data.remove("{{snakecase @key}}").ok_or_else(|| "missing field {{sanitize (snakecase @key)}}")?,
                        {{~/each}}
                    })
                }
            }
            {{~/if}}

        {{~else~}}
            {{~#if additionalProperties}}
                pub type {{camelcase name suffix}} = serde_json::Map<String, serde_json::Value>;
            {{~/if}}
        {{~/if}}
    {{~else~}}
        {{~#if (and (eq type "string") enum (not format))}}
            #[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
            pub enum {{camelcase ../name @key suffix}} {
                {{~#each enum}}
                #[serde(rename = "{{this}}")]
                {{camelcase this}},
                {{~/each}}
            }

            impl std::fmt::Display for {{camelcase ../name @key suffix}} {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}",
                        match self {
                            {{~#each enum}}
                            {{camelcase ../name @key suffix}}::{{camelcase this}} => "{{this}}",
                            {{~/each}}
                        }
                    )
                }
            }

        {{~else~}}
            {{~#if [oneOf]~}}
                #[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
                #[serde(untagged)]
                pub enum {{camelcase name suffix}} {
                {{~#each oneOf}}
                    Option{{@index}}({{>data_type required="true"}}),
                {{~/each}}
                }
            {{~else~}}
                {{~#if (eq type "string")}}

                pub type {{camelcase name suffix}}Inner =
                    {{~#if type}} {{>data_type required="true"}}
                    {{~else}} () {{~/if}}
                ;

                #[derive(Clone, Debug, PartialEq, serde::Serialize)]
                #[serde(transparent)]
                pub struct {{camelcase name suffix}}(pub {{camelcase name suffix}}Inner);

                    {{~#if pattern}}
                        lazy_static::lazy_static! {
                            static ref {{shoutysnakecase ../name "VALUE" ../suffix}}_PATTERN: regex::Regex
                                = regex::Regex::new("{{pattern}}").expect("Regex for `{{../name}}{{../suffix}}`'s value");
                        }

                    {{~/if}}

                    impl {{camelcase name suffix}} {
                        pub fn into_inner(self) -> {{camelcase name suffix}}Inner {
                            self.0
                        }
                    }

                    impl<'de> serde::Deserialize<'de> for {{camelcase name suffix}} {
                        fn deserialize<D>(d: D) -> Result<Self, D::Error>
                        where
                            D: serde::Deserializer<'de>,
                        {
                            let res = {{camelcase name suffix}}Inner::deserialize(d)?;

                            {{~#if pattern}}
                                {{>validation object=this name=name required="true" key="value" suffix=suffix var_name="res"}}
                            {{~/if}}

                            Ok(Self(res))
                        }
                    }

                    impl core::fmt::Display for {{camelcase name suffix}} {
                        fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
                            write!(f, "{}", self.0)
                        }
                    }

                    impl actix_web::body::MessageBody for {{camelcase name suffix}} {
                        type Error = <{{camelcase name suffix}}Inner as actix_web::body::MessageBody>::Error;

                        #[inline]
                        fn size(&self) -> actix_web::body::BodySize {
                            <{{camelcase name suffix}}Inner as actix_web::body::MessageBody>::size(&self.0)
                        }

                        #[inline]
                        fn poll_next(
                            mut self: std::pin::Pin<&mut Self>,
                            _cx: &mut std::task::Context<'_>,
                        ) -> std::task::Poll<Option<Result<actix_web::web::Bytes, Self::Error>>> {
                            let mut inner = self.as_mut();
                            let inner = std::pin::Pin::new(&mut inner.0);
                            <{{camelcase name suffix}}Inner as actix_web::body::MessageBody>::poll_next(inner, _cx)
                        }

                        #[inline]
                        fn try_into_bytes(self) -> Result<actix_web::web::Bytes, Self> {
                            <{{camelcase name suffix}}Inner as actix_web::body::MessageBody>::try_into_bytes(self.0)
                                .map_err(Self)
                        }
                    }

                {{~else~}}

                pub type {{camelcase name suffix}} =
                    {{~#if type}} {{>data_type required="true"}}
                    {{~else}} ()
                    {{~/if}};


                {{~/if}}

            {{/if}}
        {{/if}}
    {{~/if}}
{{~/if}}
