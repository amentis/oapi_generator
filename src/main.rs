use anyhow::Result;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "openapi_generator",
    about = "Generate code from OpenAPI specifications"
)]
struct Cli {
    /// Absolute path to all OAPI Specifications e.g. /home/user/oapispecs
    openapi_absolute: PathBuf,
    /// Relative path from the root e.g. oapi.yaml or service_folder/oapi.yaml
    openapi_spec_relative: String,
    /// Destination of the generated code
    destination: PathBuf,
}

fn main() -> Result<()> {
    pretty_env_logger::init();
    let args = Cli::from_args();
    oapi_generator::generate_oapi_server_stubs(
        args.openapi_absolute,
        args.openapi_spec_relative.as_str(),
        args.destination,
    )
}
